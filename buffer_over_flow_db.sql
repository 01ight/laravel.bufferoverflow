-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 24 Février 2016 à 21:15
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `buffer_over_flow_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `badges`
--

CREATE TABLE IF NOT EXISTS `badges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `badge_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `badge_icon` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `badges`
--

INSERT INTO `badges` (`id`, `badge_description`, `created_at`, `updated_at`, `badge_icon`) VALUES
(1, 'Ut laborum hic temporibus cupiditate ut voluptate dolorem. Error exercitationem amet minima fugiat aut consequuntur aliquam accusamus. Ut et error voluptatem necessitatibus labore dolores. Ab earum ullam adipisci.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-award24'),
(2, 'Aut dignissimos est et eligendi. Consequatur est perspiciatis dicta suscipit beatae architecto. Ratione necessitatibus quia rerum ducimus quisquam voluptate rerum.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-medal67'),
(3, 'Quos nam qui aut nesciunt molestiae et. Iste dolorem quo voluptatem vel. Unde commodi molestias quis sit unde.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-premium1'),
(4, 'Qui similique commodi fugiat voluptas omnis labore explicabo. Et et eum quam officiis fugit.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-shield73'),
(5, 'Dolores sunt ratione et iure quam ut. Assumenda molestiae qui est commodi necessitatibus reiciendis dolorum. Fuga omnis molestiae laborum nesciunt tenetur. Atque exercitationem quo ipsa fuga molestiae.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-sports27'),
(6, 'Repudiandae amet omnis fugiat veritatis sint. Ipsum laboriosam magnam similique ab. Laudantium asperiores totam ad dolorum neque dolores. Ut veritatis reprehenderit tempore vel doloribus.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-star16'),
(7, 'Sequi cumque accusantium ea autem ut officiis aut sint. Odit provident ut voluptatum. Ipsa ex quis reprehenderit a accusamus quod culpa.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-trophy42'),
(8, 'Eos doloribus dolores quasi. Sit amet beatae nisi omnis alias exercitationem. Quia qui aliquid reiciendis consequuntur dolor ullam. Voluptas autem reiciendis doloremque et.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-trophy75'),
(9, 'Tempora fugit sunt ipsa hic et quas. Quas vitae dolor repellat omnis. Nulla incidunt voluptatem autem eligendi minus quia. Eveniet cum voluptatem hic iure at. Rerum qui explicabo repudiandae omnis.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-wax1'),
(10, 'Hic ex voluptas vitae. Voluptatibus sint ut earum aut illum voluptate pariatur. Qui vel beatae perspiciatis ea.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'flaticon-winners1');

-- --------------------------------------------------------

--
-- Structure de la table `badge_user`
--

CREATE TABLE IF NOT EXISTS `badge_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `badge_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Contenu de la table `badge_user`
--

INSERT INTO `badge_user` (`id`, `user_id`, `badge_id`) VALUES
(1, 1, 2),
(5, 1, 3),
(6, 51, 3),
(7, 51, 3),
(8, 51, 3),
(9, 51, 3),
(10, 51, 3),
(11, 51, 3),
(12, 51, 3);

-- --------------------------------------------------------

--
-- Structure de la table `bubble_messages`
--

CREATE TABLE IF NOT EXISTS `bubble_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bubble_message_text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Contenu de la table `bubble_messages`
--

INSERT INTO `bubble_messages` (`id`, `bubble_message_text`) VALUES
(1, 'Debitis consequatur et cum aspernatur tempora ipsum. At aut possimus perferendis sunt.'),
(2, 'Ipsam architecto deleniti fuga qui. Facilis similique eum molestiae ea est sint.'),
(3, 'Modi nemo neque et voluptas totam. Ut quas esse incidunt saepe ut minima architecto sint.'),
(4, 'Illum cupiditate earum nihil. Voluptatem adipisci at sunt eius et.'),
(5, 'Veritatis temporibus eos corporis similique reprehenderit voluptate esse. Ut non praesentium est.'),
(6, 'Beatae ut unde ut animi. Saepe quos velit iure ad beatae. Ut dicta nam et ea.'),
(7, 'Veniam ipsa est natus est eum. Aspernatur sint sit et aut.'),
(8, 'Ut aut omnis amet atque itaque dicta nostrum. Et dolorem et consequatur quaerat nobis.'),
(9, 'Voluptatem et maiores hic nobis enim in. Quibusdam hic expedita ab voluptate possimus tempora qui.'),
(10, 'Praesentium aut quis aut non quia et impedit. Accusantium earum illum voluptatem consequatur.'),
(11, 'Praesentium eos nemo soluta aut tempore ad. Ut qui dolorum eos tempora aperiam delectus corrupti.'),
(12, 'Ipsum aut voluptatum fuga impedit. Repellat dolorum aut culpa quia.'),
(13, 'Voluptas et dicta quos atque est et atque ab. Dolor quaerat qui omnis voluptate. Est est omnis id.'),
(14, 'Error id reiciendis dolor itaque inventore deserunt. Aperiam quis sapiente fugit labore amet.'),
(15, 'Vero ipsa ipsa a qui aperiam reiciendis. Nostrum consequatur voluptas et inventore dolor.'),
(16, 'Distinctio soluta et libero architecto. Voluptas ipsam officia animi sint quas dolore alias fugit.'),
(17, 'Ut sapiente numquam corporis aut. Quasi enim rerum officiis sit vel.'),
(18, 'Natus itaque molestias ut voluptatem molestiae voluptas. Aut sint in ut commodi quos molestiae ut.'),
(19, 'Iusto molestiae voluptas aut optio. Qui similique et assumenda dolores nemo expedita voluptatem.'),
(20, 'Consequatur minima ipsum vitae quam aut et vero tempore. Illo non qui in molestias illum.');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment_text` text COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `response_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`id`, `comment_text`, `question_id`, `response_id`, `created_at`, `updated_at`) VALUES
(1, 'google', 1, 3, '2015-12-22 22:32:27', '2015-12-22 22:32:27'),
(2, 'google', 1, 3, '2015-12-22 22:34:18', '2015-12-22 22:34:18'),
(3, 'qsqsd', 1, 3, '2015-12-22 22:34:39', '2015-12-22 22:34:39'),
(4, 'goog', 1, 3, '2015-12-22 22:49:48', '2015-12-22 22:49:48'),
(5, 'hdqsd', 1, 3, '2015-12-22 22:50:43', '2015-12-22 22:50:43'),
(6, 'dqsdq', 1, 3, '2015-12-22 22:51:17', '2015-12-22 22:51:17'),
(7, 'sdqs', 1, 3, '2015-12-22 22:51:53', '2015-12-22 22:51:53'),
(8, 'valar', 1, 7, '2015-12-22 22:56:37', '2015-12-22 22:56:37'),
(9, 'valar', 1, 7, '2015-12-22 22:57:04', '2015-12-22 22:57:04'),
(10, 'goosq', 1, 7, '2015-12-22 23:00:08', '2015-12-22 23:00:08'),
(11, 'sdqqsdq', 1, 7, '2015-12-22 23:00:19', '2015-12-22 23:00:19'),
(12, 'qdssdd', 1, 7, '2015-12-22 23:02:29', '2015-12-22 23:02:29'),
(13, 'qsdq', 1, 7, '2015-12-22 23:03:48', '2015-12-22 23:03:48'),
(14, 'sqdqs', 1, 7, '2015-12-22 23:05:22', '2015-12-22 23:05:22'),
(15, 'xqsdqs', 1, 7, '2015-12-22 23:09:52', '2015-12-22 23:09:52'),
(16, 'qsdqd', 1, 7, '2015-12-22 23:11:56', '2015-12-22 23:11:56'),
(17, 'dqsdq', 1, 7, '2015-12-22 23:13:12', '2015-12-22 23:13:12'),
(18, 'sdqsd', 1, 7, '2015-12-22 23:14:03', '2015-12-22 23:14:03'),
(19, 'sqqsqdsqd', 1, 6, '2015-12-22 23:15:02', '2015-12-22 23:15:02'),
(20, 'sqdqdsqsdq', 1, 7, '2015-12-22 23:15:21', '2015-12-22 23:15:21'),
(21, 'res 1 com1', 2, 8, '2015-12-23 00:49:27', '2015-12-23 00:49:27'),
(22, 'res 2 comm1', 2, 9, '2015-12-23 00:49:46', '2015-12-23 00:49:46'),
(23, 'res 1 coom 2', 2, 8, '2015-12-23 00:50:43', '2015-12-23 00:50:43'),
(24, 'res 2 comm2', 2, 9, '2015-12-23 00:51:25', '2015-12-23 00:51:25'),
(25, 'dqsdqs', 5, 0, '2015-12-23 01:44:16', '2015-12-23 01:44:16'),
(26, 'resp 1 com 1', 5, 10, '2015-12-23 01:46:32', '2015-12-23 01:46:32'),
(27, 'resp 2 com1', 5, 11, '2015-12-23 01:47:12', '2015-12-23 01:47:12'),
(28, 'resp 2 com 2', 5, 11, '2015-12-23 01:47:21', '2015-12-23 01:47:21'),
(29, 'dcsqfsdf', 1, 0, '2015-12-24 21:23:35', '2015-12-24 21:23:35'),
(30, 'googel', 52, 0, '2015-12-25 01:17:40', '2015-12-25 01:17:40'),
(31, 'this is a comment', 52, 13, '2015-12-25 01:32:12', '2015-12-25 01:32:12'),
(32, 'comment for q 24', 24, 0, '2015-12-28 13:26:35', '2015-12-28 13:26:35');

-- --------------------------------------------------------

--
-- Structure de la table `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `favorite_value` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `favorites`
--

INSERT INTO `favorites` (`id`, `favorite_value`, `created_at`, `updated_at`, `user_id`, `question_id`) VALUES
(2, 1, '2015-12-25 21:52:33', '2015-12-25 21:52:33', 1, 51),
(4, 1, '2015-12-25 22:13:40', '2015-12-25 22:13:40', 1, 24),
(5, 1, '2015-12-26 02:55:47', '2015-12-26 02:55:47', 1, 39),
(9, 1, '2015-12-28 15:01:29', '2015-12-28 15:01:29', 1, 52),
(10, 1, '2015-12-29 00:38:01', '2015-12-29 00:38:01', 51, 53);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_11_184738_create_questions_table', 1),
('2015_12_21_204937_create_tags_table', 1),
('2015_12_21_210837_question_tag', 1),
('2015_12_22_025753_create_responses_table', 1),
('2015_12_22_210307_create_comments_table', 2),
('2015_12_23_030308_create_votes_table', 3),
('2015_12_24_010839_votes_add_question_id_response_id', 4),
('2015_12_24_025009_add_validated_to_questions_table', 5),
('2015_12_24_030424_add_views_to_questions_table', 6),
('2015_12_24_224239_add_last_login_date_to_users_table', 7),
('2015_12_24_230023_add_first_name_and_last_name_to_users_table', 8),
('2015_12_24_230416_add_avatar_to_users_table', 9),
('2015_12_25_213929_create_favorites_table', 10),
('2015_12_25_220005_create_favorite_question_table', 11),
('2015_12_25_222709_add_user_id_and_question_id_to_favorites_table', 12),
('2015_12_28_170653_create_badges_table', 13),
('2015_12_28_170910_create_badge_user_pivot_table', 14),
('2015_12_28_174650_add_badge_icon_to_badges_table', 15),
('2015_12_29_011717_create_bubble_messages_table', 16);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `validated` tinyint(1) NOT NULL DEFAULT '0',
  `views` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `questions_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=55 ;

--
-- Contenu de la table `questions`
--

INSERT INTO `questions` (`id`, `question_title`, `question_body`, `user_id`, `created_at`, `updated_at`, `validated`, `views`) VALUES
(1, 'Libero necessitatibus quis quia ut et voluptas.', 'Blanditiis inventore doloribus dolorum laudantium. Repellat quis earum quaerat est modi et. Ullam rerum qui quasi quibusdam. Nesciunt aut rerum dolores reiciendis vitae reprehenderit eum est. Enim porro cum vero natus vero blanditiis unde.', 1, '2015-12-22 17:08:41', '2015-12-24 22:27:38', 0, 14),
(2, 'Voluptatem fugit unde ut magnam iusto qui.', 'Laudantium fugit aperiam expedita cupiditate quas ex. Dignissimos expedita aperiam quibusdam. Aut natus porro corrupti velit. Fugit consequuntur enim iste voluptatem sed eos.', 2, '2015-12-22 17:08:41', '2015-12-24 17:15:21', 0, 2),
(3, 'Laudantium voluptatibus aliquid voluptate sunt.', 'Atque distinctio sint nulla doloribus temporibus. Omnis vitae a ipsam est. Dolores ullam voluptatem porro. Deleniti et commodi laborum nisi commodi nihil sed. Praesentium aut quas esse ipsam enim id doloremque.', 3, '2015-12-22 17:08:41', '2015-12-24 17:15:06', 0, 1),
(4, 'Qui sit eaque accusantium.', 'Molestias tenetur quo eos labore. Excepturi explicabo deleniti illum quasi ex provident. Modi aut labore dolorem et aliquam. Dolore eum dolorum animi maxime dolore.', 4, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(5, 'Sed doloribus rerum a. In et aut eum sunt soluta.', 'Cumque nemo est repellat architecto inventore repellat rerum quia. Ducimus modi voluptas nihil iure. Error et molestiae minima amet sit explicabo non. Vel omnis dicta velit fugiat dolorem dignissimos.', 5, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(6, 'Sed sunt tenetur ab ut.', 'Doloremque itaque libero libero iusto velit. Dignissimos rem eligendi delectus id minima in. Quidem dolores asperiores facilis aliquid. Dolorem aut molestiae in optio odit.', 6, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(7, 'Soluta possimus ipsa officia.', 'Facilis non assumenda animi dolorum. Alias et ut at ipsa explicabo rem minus ratione. Qui sed sed officia et in. Quos soluta autem quis quasi ut. Ipsam accusantium est omnis aut. Unde ducimus porro dolores debitis perspiciatis dolorum.', 7, '2015-12-22 17:08:41', '2015-12-25 00:46:06', 0, 1),
(8, 'Aperiam quo culpa qui laborum quod.', 'Accusamus blanditiis aut temporibus et repellat deleniti quo. In facilis porro illo exercitationem. Eius iure quae reiciendis debitis id eligendi deserunt. Iure hic quia eos eos voluptatem dolores. Molestiae accusamus nobis aut ut suscipit quia aliquam. Aut aut et dicta ea quasi.', 8, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(9, 'Quia et illum nihil quo totam nemo.', 'Tenetur nisi illo quis odio velit dolorem. Eaque ut itaque officiis et ut quos. Excepturi est illo fugiat. Hic quisquam ducimus et tempora et. Est quae et sit qui.', 9, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(10, 'Ut soluta dolor in nesciunt exercitationem.', 'Hic veniam iste incidunt omnis. Aut placeat autem dolorem. Ducimus et nihil expedita commodi. Repellat vero rerum facere eaque quas aut suscipit. Sit deserunt eos voluptatum nam est. Molestiae omnis repudiandae autem quaerat rerum magnam qui possimus.', 10, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(11, 'Et laborum ut ad molestiae deserunt ut voluptas.', 'Provident non exercitationem fugit quo ex. Error deleniti ipsam doloremque commodi sint dicta deleniti minima. A est quia enim ut. Quia illum laudantium temporibus qui esse et adipisci rerum. Necessitatibus facilis dicta accusamus molestiae id. Eveniet non molestias aut accusamus dolorem amet commodi.', 11, '2015-12-22 17:08:41', '2015-12-24 22:24:28', 0, 6),
(12, 'Magni numquam facilis quia voluptas voluptatem.', 'Enim nesciunt nostrum consequuntur voluptates iure ut perspiciatis. Officia laborum ut blanditiis aspernatur aut consequatur. Quidem dolor sit illum similique qui.', 12, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(13, 'Sunt sit aut modi laborum velit impedit omnis.', 'Sint exercitationem autem ut qui in. Nobis ea optio et. Ut a ut optio exercitationem et. Et quam quia ut corporis. Vero tenetur itaque incidunt ad est.', 13, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(14, 'Numquam sint dolore est minima.', 'Distinctio ut consequatur voluptas ratione. Vitae ipsam numquam tenetur possimus atque eaque officia. Nesciunt recusandae maxime magni odio. Non aut molestias voluptas maiores quo occaecati.', 14, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(15, 'Et deleniti maxime et quo ipsam eligendi.', 'Omnis nihil quia tempore odit sunt porro nemo. Fugiat quibusdam repellendus consequuntur quia. Voluptates quasi eligendi enim tempore. Veritatis atque rerum laudantium.', 15, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(16, 'Quo laboriosam accusamus doloribus officia.', 'Qui quo corporis in sunt quos culpa. Omnis et voluptatem et eius laudantium maxime. Reiciendis harum id dolores sunt consequatur. Quia sed expedita velit facilis quod numquam omnis.', 16, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(17, 'Dolore eaque officiis temporibus eveniet nam.', 'Alias maxime quo molestiae officia perspiciatis reiciendis. Qui temporibus quia aut laborum blanditiis quis eos dignissimos. Delectus aut facere aut temporibus atque deserunt est. Eos voluptas facilis cumque illo omnis facere aut. Ducimus ut mollitia velit dolor sint dignissimos et.', 17, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(18, 'Ea magni dignissimos numquam nihil.', 'Facilis expedita aut itaque molestiae. Nihil veritatis quia qui consequuntur sapiente. Exercitationem sequi repudiandae tempora facilis doloremque eos. Et culpa voluptatem necessitatibus. Occaecati reiciendis illum quibusdam est nam perspiciatis.', 18, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(19, 'Ut est perferendis modi est assumenda cumque.', 'Placeat vel aut voluptatem. Quo quibusdam fugiat sequi placeat mollitia quaerat voluptatum sed. Dolores omnis repellat ut. Distinctio fugiat commodi architecto totam alias eum quam. Omnis quia odio dolorum nulla rerum et dolores quia. Quaerat occaecati nihil laudantium rem tenetur et.', 19, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(20, 'Sunt tempore expedita ex.', 'Aperiam ea facilis porro eligendi. Ea aut blanditiis et modi sequi officia. Iusto culpa qui eligendi labore.', 20, '2015-12-22 17:08:41', '2015-12-22 17:08:41', 0, 0),
(21, 'Rem ex cupiditate at dolore.', 'Blanditiis officiis placeat laudantium adipisci ut esse. Ullam sint non doloribus incidunt architecto corporis et. Animi suscipit ut est ducimus. Totam ab nesciunt autem dolorum hic cumque. Aut laudantium repellat sed excepturi quaerat accusantium.', 21, '2015-12-22 17:08:42', '2016-02-24 18:51:28', 0, 7),
(22, 'Alias autem qui illo qui eum eaque autem.', 'Culpa omnis et aut sit voluptates a. Et sed a nemo suscipit ut. Odit exercitationem delectus eos voluptate est natus exercitationem. Eos labore doloremque iusto magnam pariatur numquam. Nam pariatur aut ex esse tempora perferendis. Sit eum qui officiis animi non et.', 22, '2015-12-22 17:08:42', '2015-12-28 13:12:57', 0, 2),
(23, 'Sapiente iste non iste consequatur laudantium a.', 'Voluptas maxime nisi quasi quas suscipit. Et quasi qui soluta ratione veniam. Sapiente sit et et vel.', 23, '2015-12-22 17:08:42', '2015-12-25 22:10:56', 0, 1),
(24, 'Dolore nulla neque illo quod et.', 'In est odit magni error rerum dolor recusandae. Aut dignissimos qui cumque hic ut. Et soluta magnam in deleniti voluptatem ullam. Delectus ea inventore magni maxime itaque.', 24, '2015-12-22 17:08:42', '2015-12-28 13:26:38', 0, 8),
(25, 'Recusandae vel dolor soluta dolor.', 'Ab sunt repellendus in qui. In iure omnis sed. Sit ex dolorem temporibus qui eum odio architecto. Sint temporibus autem delectus nihil odit rerum.', 25, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(26, 'Ipsum beatae est recusandae tempore voluptatem.', 'Ut hic assumenda perspiciatis suscipit quo. Fuga accusamus quos pariatur iste sed optio mollitia libero. Dolor numquam autem magnam quidem nemo ipsa officiis. Vel sed voluptatibus est quia deserunt provident.', 26, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(27, 'Provident soluta facilis ut.', 'Molestias in sunt molestias. Sequi incidunt quidem officia error quo dolore. Iure temporibus aut numquam voluptas eligendi natus.', 27, '2015-12-22 17:08:42', '2015-12-25 22:13:08', 0, 3),
(28, 'Dignissimos vel ut excepturi aut aut.', 'Quidem esse atque deserunt. Autem vel eveniet ut et iusto voluptatum. Aliquid quos ipsum aspernatur iste odio mollitia.', 28, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(29, 'Qui quod aut maxime beatae rerum.', 'Earum quam repellendus temporibus alias dolores adipisci. Iste eligendi et iste recusandae sed et. Praesentium quis harum et quibusdam porro.', 29, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(30, 'Nesciunt consequatur impedit et eum ad.', 'Mollitia esse ducimus placeat dolores. Enim velit aliquid quod ut nesciunt ut. Nam impedit qui excepturi suscipit consequatur ut. Maiores velit labore sit aliquid nisi beatae fugit.', 30, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(31, 'Minima atque enim quae cupiditate.', 'Explicabo rerum dolor et est cum. Repudiandae praesentium rerum aspernatur vero quia molestiae. Temporibus alias eos et eos aut.', 31, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(32, 'Ex illo voluptatum quibusdam aspernatur.', 'Consequatur autem animi molestias placeat nulla at sit. Ipsa sit iure exercitationem quia odio sed officiis. Laudantium natus quasi eum et.', 32, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(33, 'Recusandae corrupti ut doloremque et ex.', 'Omnis vel laudantium porro voluptatem. Ipsam sit magnam accusantium sit. Est inventore tempore libero eum. Fugit et ut sit.', 33, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(34, 'Dolorem quasi asperiores beatae sapiente.', 'Molestias esse saepe unde ab est sint. Nam dolores ut voluptas. Voluptas necessitatibus vero libero. Et dignissimos consequatur aliquam non aut.', 34, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(35, 'Sunt blanditiis quia dolore deleniti alias.', 'Ea nihil dolor voluptatum laboriosam cum consequatur libero impedit. Qui dolore sint nemo odit dolorem sit natus magnam. Qui minima rem voluptas illum recusandae exercitationem esse.', 35, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(36, 'Facere hic ut numquam optio.', 'Autem incidunt laborum magni nihil. Quia consectetur tempore debitis sapiente totam esse deserunt. Reiciendis fugit voluptatem fugit et reiciendis et.', 36, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(37, 'Ex vitae ut dolorum labore ab itaque.', 'Incidunt quam culpa dolorem ad reiciendis laborum iste repellendus. Nobis animi cupiditate sint est velit quasi. Eaque ratione perspiciatis repudiandae sit fuga. Quisquam quidem magnam sunt.', 37, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(38, 'Quia labore tenetur animi impedit qui.', 'Neque sequi consequatur dicta sapiente. Esse sed sit distinctio dolor. Alias commodi exercitationem vero eum qui totam.', 38, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(39, 'Iusto ipsum explicabo omnis aliquid.', 'Fuga perspiciatis quia expedita ducimus minima et fugiat quo. Quis quasi tempora explicabo tempora quasi vel provident quia. Tenetur ipsa ut quam expedita totam sit voluptatem aut. Dolorem voluptatibus enim hic voluptates voluptatem nemo corporis.', 39, '2015-12-22 17:08:42', '2015-12-26 02:55:34', 0, 1),
(40, 'Qui suscipit sint quaerat non fugiat.', 'Voluptas officiis non quis ut voluptatem magni vitae illo. Est explicabo sed enim odio. Iste optio hic quia. Inventore rerum et saepe exercitationem maiores eaque voluptate. Soluta libero dolores iure praesentium nostrum. Ut fuga blanditiis dolores harum repudiandae.', 40, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(41, 'Dignissimos ipsam facere culpa id.', 'Excepturi veritatis ut corrupti eveniet. Maiores qui dolores quos aut facilis voluptas. Qui culpa mollitia eos. Aspernatur ea iure ut non.', 41, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(42, 'Inventore sint ea eos beatae aut adipisci enim.', 'Nam perspiciatis nobis cum quaerat ut qui in. Dolore ex laudantium esse consequuntur. Id veritatis aliquam voluptatem ipsum nihil ut. Eaque quia earum qui laboriosam vel est. Corporis assumenda nostrum ut dicta.', 42, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(43, 'Est doloremque et quisquam minima hic.', 'Odit ullam ipsa odio ullam voluptates ad totam. Magnam qui saepe iusto quasi. Similique reiciendis et qui consequatur et.', 43, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(44, 'Et commodi numquam eum perspiciatis est.', 'Incidunt iusto neque sunt. Explicabo optio ratione quibusdam nemo soluta eos ut sunt. Non minus rerum est quod quod. Id rerum ab nihil soluta corrupti aut id aliquid. Qui id ea aperiam velit quos.', 44, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(45, 'Consectetur omnis sequi eos ea consequuntur.', 'Ipsum perspiciatis ut sit quia est qui qui. Quibusdam laborum quasi aut. Numquam veritatis pariatur et aliquid nam. Est impedit mollitia ut qui perspiciatis.', 45, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(46, 'Aliquid commodi assumenda nihil explicabo.', 'Exercitationem alias eos sed qui in in. Quis pariatur est repudiandae accusantium quia magni. Rerum explicabo sint ab tempora adipisci ut distinctio et. Officia dignissimos nobis id rerum soluta.', 46, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(47, 'Voluptas tenetur qui non fuga natus aut dolor.', 'Sapiente vitae ipsam tenetur veniam laboriosam tenetur. Odio eaque animi autem numquam omnis. Et libero similique quam. Et rerum quia tenetur dolor ut dolores quas perferendis. Et dignissimos rerum ut saepe.', 47, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(48, 'Temporibus natus at consequatur placeat.', 'Minima laborum praesentium vero exercitationem. Consequatur aut eius et quaerat aut modi. Sint omnis reprehenderit aut. Et deleniti voluptatem quaerat quam.', 48, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(49, 'Voluptas ut officia soluta velit cum.', 'Eius qui et ut veniam. Aut mollitia adipisci eos sapiente sit. In similique cum consequatur omnis. Labore debitis qui sequi nihil sit. Veritatis nisi eos repudiandae nobis harum. Deserunt at illo optio repudiandae quod.', 49, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(50, 'Provident quam aliquid molestiae est.', 'Ut consequatur dicta quo possimus. Ipsam exercitationem nihil deleniti. Est harum et temporibus doloribus. Nam facilis doloremque optio non. Unde doloremque id repellat et aperiam dolorem tempore voluptatibus.', 50, '2015-12-22 17:08:42', '2015-12-22 17:08:42', 0, 0),
(51, 'is it good to ', 'what the fuck is that\r\n                    ', 1, '2015-12-24 23:09:58', '2015-12-26 02:56:34', 0, 21),
(52, 'this is awkward ', '\r\n                i  donst think  that is !!    ', 1, '2015-12-24 23:23:34', '2016-02-24 18:51:34', 1, 130),
(53, 'question  by  mecherrak', 'how old is the earth !!\r\n                    ', 51, '2015-12-29 00:37:40', '2016-02-24 18:51:38', 0, 5),
(54, 'what is the fuss about javascript !!', 'why every  one  today  is using js \r\n                    ', 51, '2015-12-29 00:39:32', '2016-02-24 18:56:27', 0, 3);

-- --------------------------------------------------------

--
-- Structure de la table `question_tag`
--

CREATE TABLE IF NOT EXISTS `question_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `question_tag_question_id_foreign` (`question_id`),
  KEY `question_tag_tag_id_foreign` (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Contenu de la table `question_tag`
--

INSERT INTO `question_tag` (`id`, `question_id`, `tag_id`) VALUES
(1, 51, 6),
(2, 51, 8),
(3, 51, 7),
(4, 52, 6),
(5, 52, 7),
(6, 52, 8),
(7, 21, 8),
(8, 53, 1),
(9, 53, 6),
(10, 53, 7),
(11, 53, 8),
(12, 54, 4),
(13, 54, 9),
(14, 54, 10);

-- --------------------------------------------------------

--
-- Structure de la table `responses`
--

CREATE TABLE IF NOT EXISTS `responses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `response_text` text COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Contenu de la table `responses`
--

INSERT INTO `responses` (`id`, `response_text`, `question_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, '           gooelde     ', 1, 2, '2015-12-22 17:23:52', '2015-12-22 17:23:52'),
(4, 'hfhdqshjqs', 1, 3, '2015-12-22 17:23:52', '2015-12-22 17:23:52'),
(5, '               this is a response for question 4 ', 4, 2, '2015-12-22 20:02:03', '2015-12-22 20:02:03'),
(6, '                gqsgd', 1, 2, '2015-12-22 21:41:41', '2015-12-22 21:41:41'),
(7, '                on the roof', 1, 2, '2015-12-22 21:42:01', '2015-12-22 21:42:01'),
(8, '                reponse to  question 2', 2, 2, '2015-12-23 00:48:48', '2015-12-23 00:48:48'),
(9, '                response 2 to question 2', 2, 2, '2015-12-23 00:49:12', '2015-12-23 00:49:12'),
(10, '                sqjdqs', 5, 2, '2015-12-23 01:46:02', '2015-12-23 01:46:02'),
(11, '              resp 2', 5, 2, '2015-12-23 01:46:57', '2015-12-23 01:46:57'),
(12, '                qssq', 52, 1, '2015-12-25 01:18:10', '2015-12-25 01:18:10'),
(13, '                QsQSqsQS', 52, 1, '2015-12-25 01:18:32', '2015-12-25 01:18:32'),
(14, '                this is a response for question  24', 24, 1, '2015-12-28 13:18:15', '2015-12-28 13:18:15'),
(15, '                about 5 billion years ', 53, 51, '2015-12-29 00:38:37', '2015-12-29 00:38:37');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`, `tag_description`, `created_at`, `updated_at`) VALUES
(1, 'voluptas', 'Eligendi inventore consequatur non quas optio ipsam et. Ea aut eos aut soluta ipsa.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'quia', 'Voluptas libero inventore quo facilis molestiae voluptas. Voluptas tempore aspernatur fuga voluptatem voluptates velit et omnis. Et nemo harum odit sapiente. Sit dicta omnis perspiciatis hic repellat.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'ut', 'Ad suscipit sunt dolore iure ut reprehenderit aut. Suscipit dicta tenetur ad placeat quia. Voluptas unde perspiciatis non dignissimos ea explicabo hic. Inventore quae ullam ut architecto aliquam.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'aspernatur', 'Mollitia et fuga minima et. Nulla impedit minus sit ipsum consectetur incidunt voluptas. Voluptate quis dolor sint.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'ut', 'Doloribus facere rerum dicta numquam occaecati omnis repudiandae. Ducimus ipsa consequatur sit qui ut harum magnam. Quibusdam impedit consequatur et enim ipsa iusto doloremque.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'aut', 'Et assumenda suscipit nihil repudiandae quaerat molestias eaque. Odit eligendi ipsam dolores. Temporibus dolore laborum nisi recusandae. Corrupti aut dolores est ad.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'quidem', 'Maxime nostrum totam non minus. Ut tempore aspernatur ullam similique voluptates soluta consequatur. Non iure unde alias officiis voluptatem voluptatem.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'nihil', 'Accusamus maiores culpa ipsa similique perspiciatis et. Nesciunt et ea ipsam accusantium alias nobis veniam. Pariatur omnis et aut et tempora a reiciendis. Illo id quibusdam officiis dolores.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'natus', 'Dolores nam et accusamus et. Qui animi sit illo dolore aut. Aspernatur in quas nulla voluptas. Vitae et quaerat commodi quo voluptatem.', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'quo', 'Quibusdam doloremque omnis minima veritatis. Delectus qui qui nisi non. Ea earum praesentium odit culpa repudiandae. Vero debitis soluta quos reiciendis.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login_date` timestamp NOT NULL DEFAULT '2015-12-24 21:44:48',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `last_login_date`, `first_name`, `last_name`, `avatar`) VALUES
(1, 'dstammvon', 'prau@koch.com', '$2y$10$BiPT8CCIBGUAxqkFtIIV5OJ7yvYEtMmAMhYOldVKunAN7F8eEyNxm', 'qbPOw4TdDl2KN5A12RrN9c00t4SD0lMk5EHfpMiZjofoebY02nxobvZPaULH', '2015-12-22 17:08:39', '2015-12-29 00:36:25', '2015-12-28 23:27:31', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(2, 'leonor.cummings', 'lbruen@grady.com', '$2y$10$BiPT8CCIBGUAxqkFtIIV5OJ7yvYEtMmAMhYOldVKunAN7F8eEyNxm', 'Yo2LSDtTR4', '2015-12-22 17:08:39', '2015-12-25 22:20:23', '2015-12-25 22:20:23', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(3, 'cristal.borer', 'jakubowski.margaret@frami.org', '$2y$10$Ug5Vth5k9nzDb3SkR81dSOUbiumlc6qseMWfJwWMqe15qNEJaVYl6', 'wmSwvI65nv', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(4, 'myra.bashirian', 'qgorczany@gmail.com', '$2y$10$GrDeXi9ZfYH7lKPssWqtGOfYDZy6krPMxwPFqG.6lCxOHnj4e.6jG', '59ytvRV8Gy', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(5, 'niko32', 'gabriella58@gmail.com', '$2y$10$NBjLQeZN5Tg6jT3ccOOu3.DQPrFKJdc6xoSZHPsieQ6W9NMOWqIVy', '81Ltz0FmZc', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(6, 'rosemary96', 'gbalistreri@yahoo.com', '$2y$10$nhgl3mGmhkq4EyHNP01Di.7/u5xdPk.1RL/xuCP8CEYrWswnRWk.S', 'dBlqeJJT3A', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(7, 'dion83', 'durgan.keagan@bartell.com', '$2y$10$c6E8VfwkongB49v5uIUww.hTUZKRz5xkufgFNbH4N92eXwMqdTJDq', '57C5f5eaUv', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(8, 'koepp.madeline', 'npagac@yahoo.com', '$2y$10$wGQ.3Lz.g4Prqg74kjXKTuxoN43OjgmbJzC786V1uImougXKzAXB2', 'wxljOnX8n8', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(9, 'charlie06', 'pullrich@hotmail.com', '$2y$10$Ouo7EmHByCKoRyhGqHrdqexIuZhqJzvkZ.dOln2OzOc7xwzZYpdoW', 'f03ve9YGVM', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(10, 'keebler.maybell', 'halie19@schowalter.biz', '$2y$10$Q0/Ny4LZvwoj1G10RNo/B.XdYYkVsjMTksi9yaxFf1ZgDjAgVlSzu', 'ItKc0GCiSy', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(11, 'willms.abel', 'dan.skiles@dietrich.com', '$2y$10$umrCLgoAbxhwA1B4UvkxAeslgA98LNvwLnsImefFiWtfg5ngqpvc6', 'SSDybRe2Ju', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(12, 'odell27', 'anderson.francesca@littel.info', '$2y$10$UeLMy0CVgzMX6taUJ5W3SuXuF.SkG1sRooQsXxoYn1WLYcpiitOqG', 'QCNqXx1g86', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(13, 'ryley08', 'lisa.jones@yahoo.com', '$2y$10$XS/zka4CzlHBRgi1jixzEuIyAEBJEKkfTk5IV9dpABAK1pzjUoE7O', 'IvTvzrToio', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(14, 'rod.senger', 'katherine.terry@lang.net', '$2y$10$m9L0IHpwdalAA5DMmAbRBeeb0VvWE.BpGT4WciC8kzefF5yOYfsb.', 'iXl5l2rpsM', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(15, 'davin57', 'jtillman@yahoo.com', '$2y$10$pFh34SmCt.pQHtXwc9YOweCRzipeMd/WLlY0YjCd5Zr6lW3BBT2dG', '2hk5di2XLU', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(16, 'audie87', 'mark.west@brakus.info', '$2y$10$a113SNFVBpl3KVLH/.0qMe.AHBXNtl9fdJaBRcQ.dpdQsmL0dMxXS', 'TCKO0O0xZk', '2015-12-22 17:08:39', '2015-12-22 17:08:39', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(17, 'stracke.bernie', 'zaufderhar@yahoo.com', '$2y$10$khbue.OIk3h27Kx6K7hvb.Io0j3nHppTsBPnvRNLVFUOptijo4dOS', 'yO0kxeAAQF', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(18, 'marvin.manuela', 'daniella.paucek@yahoo.com', '$2y$10$hF5B0p1sb25wHo6ULdYB0uW1k22ZubzsMqnykP7wYuAS3E6j2H8DS', 'QsBQHLBoUm', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(19, 'shanahan.dallas', 'nbashirian@gmail.com', '$2y$10$.XmX.NN0zq6Fqs7vU3ki2OIvWeVFI7yjTXno99S7AHOpwBcovkMqC', 'ReA0HqOqaJ', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(20, 'slittel', 'tyost@gmail.com', '$2y$10$z2N0oEjAJY3VVof49dEEgOx.kEpv8VWH03iknG4Gk66mbop6eQN.q', '5WXLTm3VCb', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(21, 'lubowitz.savannah', 'owalsh@barton.org', '$2y$10$f3tsjBEydVEkAtArs6gpae5UzfjDQfrR.G.DTZDVJV3uvLiGRON9O', '1M590JxXzN', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(22, 'crooks.erich', 'sasha47@von.info', '$2y$10$XheSmsbf51V6hfv6JmdjceWjmMA0.8WtTr2leQalP.p8UncAPDdhm', 'DV2wpF0rg5', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(23, 'kelsi.powlowski', 'bechtelar.krista@gmail.com', '$2y$10$HqwXCUogdYzykzPAuaYsWevNV6bZ8IYyOz4GeboIQ5XKUlghZbjHu', 'ONwOcuPpAH', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(24, 'vbahringer', 'gino33@rodriguez.com', '$2y$10$4PQvkE7GQ80FwxdOAsGCNul/.8kuiaovjIi9EuYDUeCvpnqfEp9Fu', '2NGfSHX0mc', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(25, 'tjohns', 'trent77@yahoo.com', '$2y$10$OsXN10g4A1XUeVeVoWlwZuYULeqyZg.SyjVnrPm2c2Bl1cxcNJrd.', 'DKvfAjnFtI', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(26, 'krystel01', 'brenner@hotmail.com', '$2y$10$.nC.NUfjALrT02ZMUl/.huAfJf0FDfssTe2s1fq1o4anp1a/CEjKW', 'pxjSidDfx2', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(27, 'dach.wilburn', 'nkreiger@rogahn.com', '$2y$10$W5DiKMMdaD7zJIKjXOtgHOOTJ/7tEarAB8AVja7Xm4yfwtTLUiSGm', 'Fhazq26zwC', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(28, 'abbott.mary', 'parker.ilene@yahoo.com', '$2y$10$fSEWhORw1oNMxojOwQBheOf72TEq/Cp6TEd0Gys41z73iEnR1TE/G', 'QxWnGIwmFC', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(29, 'maryjane.gerlach', 'dare.gene@gmail.com', '$2y$10$jybuu05zCkJmTMrY4nMV7.tWmyp40SRIEROHWRroVjR0Z39/.ePaK', '6t3BpBGsdp', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(30, 'jacobi.stacy', 'arnold34@becker.com', '$2y$10$fGXO0EkCfGqUzlGLL31v/ODOVkR6Sfa6982FHiGP2DCMJr15zw7MG', 'VL1VAxOYBe', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(31, 'deontae19', 'lamont.jerde@yahoo.com', '$2y$10$AXdWQgXbf07EMcJz..Jbh.at0tKpFgLphoMtfy1H5SMkpPNdV26US', 'JKXouOZw1E', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(32, 'ryder.hansen', 'quitzon.pauline@beer.org', '$2y$10$ID76tcoIlvbFBBLf4kiHa.fGAwRTCwKNJqUpkvU2GeSg3PeHGPRmC', 'fMextTRtQP', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(33, 'ashlee35', 'obraun@gmail.com', '$2y$10$1Ar6S9exq0K4.k4J0fF/BuwlUBtJlQXEn/QMp4gjsYks0zYCbR2fy', 'O8y0m1BXTu', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(34, 'dylan.kassulke', 'kunde.orin@erdman.com', '$2y$10$PWmjOgFu.on9P7DUqWVVXOFOP5KnweZ/cNeI9rJtfHKEF3KtOtD6O', 'MihPGtpr4R', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(35, 'felton.smitham', 'wilkinson.jamarcus@senger.net', '$2y$10$Avb4UBheYwFLgC1OW2KkvO3DzpzCZyg9dCpB2.3t1hDlKubSVzvwe', 'vxbhppF66v', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(36, 'kris.joelle', 'adam.stokes@gmail.com', '$2y$10$SBFEp1afb8ltJoC.aDH7h.CVFYkQLy4.fK/oHLrsL8wI5bpQeMRIu', '3jLaA1z1CK', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(37, 'halvorson.deshawn', 'amely.hilpert@padberg.org', '$2y$10$j59VN1ewGNdDmuL8Twa0ZOeTe0wRrOTjVt9J6kBZgWcQCsBYw6Dd2', 'YJkib7ZJED', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(38, 'reilly.koby', 'alia60@gmail.com', '$2y$10$EEkVK57Q2eXGyNhXz4TGIuAvsfZS4EMi2XAdpURYD.8rbzqW4qGZ.', '9rB9MlULe0', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(39, 'hosea.willms', 'ada42@gmail.com', '$2y$10$UeXh85b2gqVNs0//hn5P2uGuBf5xw3PFgRR.ez8fPiM3vhHZuyHUO', 'PjAlx20dFc', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(40, 'beahan.sigurd', 'hailie.osinski@thompson.com', '$2y$10$/W7sIHbauScsb3yKdTsdIe6NywqxZJMLmU.eDrzYPBTlttoRTGjt.', 'QQQo7PRYuM', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(41, 'mboehm', 'nona.okon@heller.com', '$2y$10$qojYh9qYSzAHPWORWMSU2O5Lsi9ltdAUI9xipubTvSaCZ9YUO42mm', 'WIyAGHZSpF', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(42, 'rrowe', 'kuvalis.reggie@towne.net', '$2y$10$SislcSW/NUDnCWNJ93u9k.fmtLLTkAa1q.eHVMYhAMKwgNn6mipsi', '6rVvVH5t3t', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(43, 'walter.cordia', 'rboehm@gutkowski.com', '$2y$10$KzZWTFab0KUVRVNu3GMAUez9sT/r43SUGoyVso4FOrsdagoXJf44.', 'yy7bwEwUpm', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(44, 'tpacocha', 'rmonahan@hotmail.com', '$2y$10$EhPOpq2Nfwj3RxrqZxWQROMBKT1ZO5PuSZLImp6AKbMfqsLG/5niu', '306oTeFEl1', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(45, 'dudley93', 'zbogisich@hotmail.com', '$2y$10$Cdti/G0pCVmvHFeBVUoLyOqo5tT1UBoHMJwzwqxeg8eK7hVvXqEBe', 'hvUHxOhTFW', '2015-12-22 17:08:40', '2015-12-22 17:08:40', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(46, 'herminio.mueller', 'cordell.harris@wintheiser.biz', '$2y$10$Jgl.cE2Ok158BP45B2Rs/eDtIvnK5xinik7tLJlC1.ra39jcRhOzS', 'sIfuYl7uCR', '2015-12-22 17:08:41', '2015-12-22 17:08:41', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(47, 'kmorar', 'lesly43@trantow.info', '$2y$10$TOpaNaDI39R9dTU1MnO9kuRrWn8SBZ.EKHx7nR/Al/bGCDF/W0ptC', 'tuFyYvXV4X', '2015-12-22 17:08:41', '2015-12-22 17:08:41', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(48, 'johnson.dawn', 'donnie38@kemmer.info', '$2y$10$s5ikyGC4ep.AI4w.x9SchuYTfusclPcJL4CzzJx9ie7x4z09qGZoq', 'W92DX14qy1', '2015-12-22 17:08:41', '2015-12-22 17:08:41', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(49, 'hagenes.cornelius', 'phauck@hotmail.com', '$2y$10$91OnBmQWiTmC/nVlGmP0fOO5WF2GMxCq6D.upSpcaXAnA0Y.htlYO', 'T9gvtvyGCw', '2015-12-22 17:08:41', '2015-12-22 17:08:41', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(50, 'demetrius57', 'andreane77@yahoo.com', '$2y$10$oio37jmcYPxXhJwQisLU/OPwPf/kvfJPQtrla1pEEe8sKJfZdc2gm', 'KOCy97P6Rh', '2015-12-22 17:08:41', '2015-12-22 17:08:41', '2015-12-24 21:44:48', 'unknown', 'unknown', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png'),
(51, 'b_mecherrak', 'interconnectid@live.com', '$2y$10$BiPT8CCIBGUAxqkFtIIV5OJ7yvYEtMmAMhYOldVKunAN7F8eEyNxm', 'GWuDJDpVk4VGsyDh2YXyP80Abp0NNvKQdoJCkwQIthKeszsCyi1dP1krVPjw', '2015-12-29 00:36:54', '2015-12-29 00:50:45', '2015-12-29 00:50:45', 'boussad', 'mecherrak', 'http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png');

-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vote_value` int(11) NOT NULL DEFAULT '0',
  `vote_type` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `question_id` int(11) NOT NULL,
  `response_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Contenu de la table `votes`
--

INSERT INTO `votes` (`id`, `vote_value`, `vote_type`, `user_id`, `created_at`, `updated_at`, `question_id`, `response_id`) VALUES
(5, 8, 0, 1, '2015-12-24 01:12:45', '2015-12-24 01:39:52', 1, 0),
(6, 6, 1, 1, '2015-12-24 01:27:34', '2015-12-24 21:24:37', 1, 3),
(7, 2, 1, 1, '2015-12-24 01:28:41', '2015-12-24 01:40:15', 1, 7),
(8, 5, 1, 1, '2015-12-24 01:29:08', '2015-12-24 01:36:04', 1, 4),
(9, 1, 6, 1, '2015-12-24 01:40:12', '2015-12-24 01:40:12', 1, 6),
(10, -1, 0, 1, '2015-12-24 01:44:52', '2015-12-24 01:44:56', 3, 0),
(11, 7, 0, 1, '2015-12-25 01:23:08', '2015-12-28 15:16:51', 52, 0),
(12, 1, 0, 1, '2015-12-25 19:27:32', '2015-12-25 19:27:37', 21, 0),
(13, 3, 0, 1, '2015-12-26 01:04:05', '2015-12-26 02:56:38', 51, 0),
(14, 6, 0, 1, '2015-12-26 02:55:39', '2015-12-26 02:55:42', 39, 0);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `question_tag`
--
ALTER TABLE `question_tag`
  ADD CONSTRAINT `question_tag_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `question_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
