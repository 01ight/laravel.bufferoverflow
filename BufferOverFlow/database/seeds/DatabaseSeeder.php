<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(TagTableSeeder::class);
        factory(App\User::class, 50)->create()->each(function($u) {
            $u->questions()->save(factory(App\Question::class)->make());
        });

        Model::reguard();
    }
}
