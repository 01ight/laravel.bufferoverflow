<?php

use Illuminate\Database\Seeder;

class BubbleMessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

        $faker = Faker\Factory::create();
        for($i=0 ;  $i<10 ; $i++)
            DB::table('bubble_messages')->insert([
                'bubble_message_text' => $faker->text(100),
            ]);
    }
}
