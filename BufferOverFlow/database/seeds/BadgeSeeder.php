<?php

use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        for($i=0 ;  $i<10 ; $i++)
            DB::table('badges')->insert([
                'badge_description' => $faker->paragraph(),
            ]);
    }
}
