<?php

use Illuminate\Database\Seeder;
use  Faker\Generator ;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        for($i=0 ;  $i<10 ; $i++)
        DB::table('tags')->insert([
            'tag_name' => $faker->word(),
            'tag_description' => $faker->paragraph(),
        ]);
    }
}
