<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuestionTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_tag' , function ( Blueprint $table ) {

            $table->increments('id') ;
            $table->integer('question_id')->unsigned();
            $table->integer('tag_id')->unsigned() ;

        } ) ;

        Schema::table('question_tag', function($table) {

            $table->foreign('question_id')->references('id')->on('questions');
            $table->foreign('tag_id')->references('id')->on('tags');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question_tag') ;
    }
}
