<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotesAddQuestionIdResponseId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("votes", function($table) {
            $table->integer('question_id');
            $table->integer('response_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('votes', function ($table) {
            $table->dropColumn('question_id');
            $table->dropColumn('response_id');
        });
    }
}
