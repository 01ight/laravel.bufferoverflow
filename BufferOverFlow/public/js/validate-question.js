$( document ).ready(function() {
    $('.validate-question').on('click' ,  function  () {

        // question  is validated !!
        if($(this).hasClass('validated')) return  ;

        var CSRF_TOKEN = $('input[name="_token"]').val();
        var question_id  = $("input#question_id").val() ;

        submitValidation($(this), question_id,CSRF_TOKEN);
    });

    function submitValidation(voteEl,  question_id , CSRF_TOKEN ) {

        console.log(CSRF_TOKEN + " " + question_id) ;
        $.ajax({
            type: "PUT",
            url: '/question/'+question_id,
            data: {
                question_id :question_id,
                _token: CSRF_TOKEN
            },
            success: function (val) {
                console.log(val) ;
                showValid(voteEl) ;
            },
            error: function (e) {
                console.log(e)
            }
        })
    }

    function  showValid(voteValueEl) {
        console.log(voteValueEl)
        voteValueEl.addClass('validated');
    }

})