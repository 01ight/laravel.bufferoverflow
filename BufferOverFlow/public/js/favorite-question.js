$( document ).ready(function() {
    $(".favorite-question").on('click' ,  function  () {

        // question  is favorited !!
        var choice = 1 ;
        if($(this).hasClass('fa-star')) choice =0  ;

        var CSRF_TOKEN = $('input[name="_token"]').val();
        var question_id  = $("input#question_id").val() ;

        submitFavorite($(this), question_id,CSRF_TOKEN, choice);

    });

    function submitFavorite(favoriteEl,  question_id , CSRF_TOKEN , choice) {

        console.log(CSRF_TOKEN + " " + question_id) ;
        $.ajax({
            type: "POST",
            url: '/favorite/add',
            data: {
                choice : choice ,
                question_id :question_id,
                _token: CSRF_TOKEN
            },
            success: function (val) {
                console.log(val) ;
                showValid(favoriteEl, choice) ;
            },
            error: function (e) {
                console.log(e)
            }
        })
    }

    function  showValid(favoriteEl, choice) {
        console.log(favoriteEl)
        if(choice)
        {
            favoriteEl.removeClass('fa-star-o');
            favoriteEl.addClass('fa-star');
        }else
        {
            favoriteEl.removeClass('fa-star');
            favoriteEl.addClass('fa-star-o')    ;
        }

    }

})