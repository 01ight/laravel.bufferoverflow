<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{

    protected $fillable = [
        'user_id',
        'badge_id'
    ];

    public function  users () {
        return  $this->belongsToMany('App\User') ;
    }
}
