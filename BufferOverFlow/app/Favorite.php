<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected  $fillable = [
       'favorite_value'
    ];

    public function  question() {
        return  $this->belongsTo('App\Question') ;
    }
     public function  user()
     {
         return  $this->belongsTo('App\User') ;
     }
}
