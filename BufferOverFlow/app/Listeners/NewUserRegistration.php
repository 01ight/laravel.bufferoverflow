<?php

namespace App\Listeners;

use App\Events\UserHasSignedUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserRegistration
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**x
     * Handle the event.
     *
     * @param  UserHasSignedUp  $event
     * @return void
     */
    public function handle(UserHasSignedUp $event)
    {
        //
    }
}
