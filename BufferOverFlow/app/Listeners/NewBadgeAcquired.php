<?php

namespace App\Listeners;

use App\Badge;
use App\Events\UserReachedMasterLevel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewBadgeAcquired
{
    public $badge;
    public $user ;

    /**
     * Create the event listener.
     * @param Badge $badge
     */
    public function __construct( Badge $badge )
    {


        $this->badge= $badge->find(3)  ;
    }

    /**
     * Handle the event.
     *
     * @param  UserReachedMasterLevel  $event
     * @return void
     */

    public function handle(UserReachedMasterLevel $event)
    {
        $this->user  = $event->user;
        $this->user->badges()->attach($this->badge)   ;
        $event->user->save() ;
    }

}
