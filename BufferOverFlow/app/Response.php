<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{

    protected $fillable  = [
        'response_text',
        'question_id',
        'user_id'
    ] ;

    public function question()
    {
        return  $this->belongsTo('App\Question') ;
    }
    public function  user()
    {
        return $this->belongsTo('App\User') ;
    }

    public function comments() {
        return  $this->hasMany('App\Comment') ;
    }
}
