<?php
namespace App\Helpers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;

class Paginator extends Collection {

    private $pageNr;
    private $perPage;
    private $all;

    public function paginate($perPage)
    {
        $this->perPage = (int) $perPage;
        $this->pageNr = (int) $this->getCurrentPage();

        $this->all = $this->all();
        $this->items = array_slice($this->all(), $this->getCurrentPage()*$perPage-$perPage, $this->perPage);
        return $this;
    }

    public function getCurrentPage()
    {
        $input = Input::get('page');
        if($input==null)
            return 1;
        return $input;
    }


    public function getLastPage()
    {
        return(intval(ceil(count($this->all)/$this->perPage)));
    }

    public function getUrl($page)
    {
        return url('question?page='.$page);
    }


}