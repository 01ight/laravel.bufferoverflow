<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App ;
use Auth ;
class VoteController extends Controller
{


    public function  add(Request $request)
    {

        if(Auth::check())
        {
            $input = $request->input('vote_value') ;
            $response_id = $request->input('response_id') ;
            $question_id = $request->input('question_id') ;

            $val= 1 ;
            if($input == -1) $val = -1 ;
            $con = ['question_id' => $question_id , 'response_id' => $response_id] ;
            $vote =  App\Vote::where($con)->first() ;

             if ($vote === null)
                 $vote = new App\Vote(
                     [
                         'vote_type' => $response_id,
                         'vote_value'=> $val ,
                         'question_id' => $question_id,
                         'response_id' => $response_id,
                         'user_id' => Auth::id()

                     ]
                 ) ;
            else {
                $vote->vote_type = ($response_id == 0 ) ? 0 :1 ;
                $vote->vote_value += $val ;
                $vote->question_id = $question_id  ;
                $vote->response_id = $response_id  ;
                $vote->user_id = Auth::id() ;
            }




            $q = App\Question::find($question_id) ;
            $vote->question()->associate($q) ;

            $vote->save() ;
            return  $vote->vote_value ;
        }
    }
}
