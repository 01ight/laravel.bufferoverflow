<?php

namespace App\Http\Controllers;

use App\Events\UserReachedMasterLevel;
use App\Question;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User ;
use Auth ;
class ProfileController extends Controller
{



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::findOrFail(Auth::id()) ;

        $hasErrors =0 ;

        if(empty($request->input('login'))) $hasErrors=1 ;

        if(!empty($request->input('previews_password')))
        {
            if (! \Hash::check($request->input('previews_password'), $user->password))  $hasErrors =1 ;

            if($request->input('new_password') != $request->input('new_password_confirm') ) $hasErrors = 1 ;

        }

        if($hasErrors == 1)
            redirect()->route('profile.edit')->withErrors('editProfileError',  'there are errors while editing your profile !!') ;
        else {
            if(!empty($request->input('first_name'))) $user->first_name = $request->input('first_name') ;
            if(!empty($request->input('last_name'))) $user->last_name = $request->input('last_name') ;
            if(!empty($request->input('avatar'))) $user->avatar = $request->input('avatar') ;

            $user->login = $request->input('login') ;
            $user->password = $request->input('new_password');
            $user->save() ;

            return  redirect()->route('profile.show')->with('updatedProfile', 'Your profile has been updated successfully !! ');
            }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {



        $user  = User::findOrFail(Auth::id()) ;

        $this->rewardUserIfDeserved($user) ;

        $questions = $user->questions()->orderBy('questions.created_at', 'DESC')->limit(5)->get(array('questions.question_title', 'questions.created_at', 'questions.id'))->toArray() ;
        $responses = $user->responses()->orderBy('responses.created_at', 'DESC')->limit(5)->get(array('responses.response_text', 'responses.created_at', 'responses.question_id'))->toArray() ;
        $favorites = $user->favorites()->get(array('favorites.question_id'))->toArray() ;
        $favs= [] ;
        for($i =0  ;  $i<count($favorites); $i++)
            array_push($favs,  $favorites[$i]['question_id']) ;



        $badges  = $user->badges()->get() ;
        $favs =  Question::whereIn('id' , $favs)->get() ;

        return  view('user.profile')->with('user', $user)
                                    ->with('questions' , $questions)
                                    ->with('responses' ,$responses)
                                    ->with('badges' ,$badges)
                                    ->with('favorites' ,$favs) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user  = User::findOrFail(Auth::id()) ;
        return  view('user.edit-profile')->with('user' , $user) ;
    }

    public function rewardUserIfDeserved(User $user ) {
        if($user->questions()->count() > 0 )
        \Event::fire(new UserReachedMasterLevel($user));
    }
}
