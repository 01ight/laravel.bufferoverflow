<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests;
use App ;
use Auth  ;

use Validator ;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $current_question ;
    public function index()
    {
        $questions= Question::orderBy('created_at', 'desc')->paginate(10) ;
        return  view('questions.all-questions' , compact('questions')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        if(Auth::check())
        {
            $tags  = App\Tag::all() ;
            return view('questions.create-question', compact('tags')) ;

        }
        return \Redirect::intended('auth/login')->with('mustLogIn', 'login to be able to ask a question !!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'question_title' => 'required',
            'question_body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('question/create')
                ->withErrors($validator)
                ->withInput();
        }


        $question = new App\Question() ;

        $question->question_title = $request->all()['question_title']  ;
        $question->question_body = $request->all()['question_body']  ;
        $question->user_id =Auth::id() ;
        $question->created_at = \Carbon\Carbon::now() ;

        $question->save();
        $question->tags()->attach($request->input('tags')) ;

        return  redirect('question/create')->with('questionCreated' , 'question created !! ') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->current_question = $id ;
        $question = App\Question::find($id)  ;

        $tags =  $question->tags()->get()->toArray();

        $categories = $question->tags->modelKeys();
        $relatedQuestions = Question::whereHas('tags', function ($q) use ($categories) {
            $q->whereIn('tags.id', $categories);
        })->where('id', '<>', $id)->get();

        $owner = $question->user()->first()->toArray() ;

        $favorited = 0;
        $user_f =  App\Favorite::where(['user_id' => Auth::id(), 'question_id'=> $id])->first() ;
        if($user_f) $favorited =1 ;

        $conditions = ['questions.id' => $id ,  'comments.response_id' => 0] ;
        $question_comments =\DB::table('users')
                                ->join('questions', 'users.id', '=', 'questions.user_id')
                                ->join('comments', 'comments.question_id', '=', 'questions.id')
                                ->select('users.login', 'comments.comment_text', 'comments.created_at', 'comments.updated_at')
                                ->where($conditions)
                                ->get() ;
        $question_vote =  App\vote::select('vote_value')
                                    ->where('question_id' , '=',  $id)
                                    ->first();
        if($question_vote === null) $question_vote =0 ;
        else $question_vote = $question_vote->toArray()['vote_value'] ;

        $responses =\DB::table('users')
            ->join('responses', 'users.id', '=', 'responses.user_id')
            ->join('questions', 'users.id', '=', 'questions.user_id')
            ->select('users.login', 'responses.created_at', 'responses.id','responses.updated_at',  'responses.response_text')
            ->where('responses.question_id' , $id)
            ->groupBy('responses.id')
            ->get() ;

         $response_comments =\DB::table('users')
                         ->join('responses', 'users.id', '=', 'responses.user_id')
                         ->join('comments', 'comments.response_id', '=', 'responses.id')
                         ->select('users.login', 'users.avatar', 'responses.id', 'comments.comment_text', 'comments.created_at', 'comments.updated_at')
                         ->where('responses.question_id' , $id)
                         ->get() ;


        $question->views += 1 ;
        $question->save() ;
        return view('questions.question')
                    ->with('question', $question)
                    ->with('question_comments', $question_comments)
                    ->with('question_vote', $question_vote)
                    ->with('owner', $owner)
                    ->with('responses' , $responses)
                    ->with('response_comments',  $response_comments)
                    ->with('favorited' , $favorited)
                    ->with('relatedQuestions' , $relatedQuestions)
                    ->with('tags' , $tags) ;

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::check())
        {
            if($this->isUserOwnerOfQuestion(Auth::id() , $request->input('question_id')) )
            {
                $q = Question::findOrFail($request->input('request_id')) ;
                $q->validated = 1 ;
                $q->save() ;
            }
            else return  'this question  is not yours,  you cannot validate it  !!' ;
        }
    }

    private function isUserOwnerOfQuestion( $user_id, $question_id)
    {
        $user = App\User::findOrFail($user_id) ;
        $user_questions = $user->questions()->get(array('questions.id'))->toArray() ;

        for($i =0 ; $i<count($user_questions) ; $i++)
        {
            if(in_array($question_id, $user_questions[$i])) {
               return  true  ;
            }
        }
        return  false  ;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function validateQuestion(Request $request ) {

        if(Auth::check())
        {
            $q = Question::findOrFail($request->input('request_id')) ;
            $q->validated = 1 ;
            $q->save() ;
        }

    }
    public function  questionsWithTag($tagId ) {

        $tag = App\Tag::findOrFail($tagId) ;

        $qq = $tag->questions()->paginate(10);

        return  view('questions.all-questions' )->with('questions', $qq) ;
    }
    public function  relatedQuestion ()
    {

    }
}
