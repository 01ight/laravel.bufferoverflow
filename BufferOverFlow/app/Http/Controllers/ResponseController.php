<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App ;
use Auth ;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check())
        {

            $this->validate($request , [
                'response_text' => 'required',
                'question_id' => 'required'
            ]) ;
            $response = new App\Response() ;

            $response->response_text=  $request->all()['response_text'] ;
            $response->question_id = $request->all()['question_id'] ;
            $response->user_id = Auth::id();
            $question = App\Question::find($request->input('question_id')) ;
            $response->question()->associate($question) ;
            $response->save() ;


            return  redirect()->route('question.show', [$request->all()['question_id'] ])
                ->with('responseMade', 'the response was saved !!') ;
        }


        return  redirect('auth/login')
            ->with('mustLogin', 'you have to authenticated to post an  answer !!' ) ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
