<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Question;

class FavoriteController extends Controller
{

    public function add(Request $request)
    {
        if (\Auth::check()) {
            $question_id = $request->input('question_id');
            $q = Question::findOrFail($question_id);


            if ($request->input('choice') == 0) {
                $f = Favorite::where(['question_id' => $question_id, 'user_id' => \Auth::id()]);
                $f->delete();
                return 'deleted' ;
            }

            $f=  new Favorite([
                'favorite_value' => 1
            ]);
            $u = User::findOrFail(\Auth::id());

            $f->question()->associate($q);
            $f->user()->associate($u);
            $f->save();

            return $f->user()->get();
        } else return 'login  to add questions to your favorite';
    }
}
