<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth ;
use App;

class CommentController extends Controller
{

    public function  show($id)
    {
        if(Auth::check)
        {
        }

        return  \Redirect::intended('/home') ;
    }

    public function  store(Request  $request)
    {
        if(Auth::check())
        {
                $this->validate($request, [
                    'comment_text' => 'required'
                ]) ;

                $comment = new Comment([
                    'comment_text' => $request->input('comment_text'),
                    'question_id' => $request->input('question_id'),
                    'response_id' => $request->input('response_id'),
                ]) ;

                $comment->save() ;

                $commentData = [];

                $commentData['comment_text'] = $comment->comment_text ;
                $commentData['comment_created_at'] = $comment->created_at ;
                $commentData['comment_updated_at'] = $comment->updated_at ;
                $commentData['comment_author'] = App\User::find(Auth::id())->login ;

                return  json_encode($commentData) ;
        }

    }
}
