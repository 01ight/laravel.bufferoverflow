<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use  Illuminate\Support\Facades\Hash ;

DB::enableQueryLog();



Route::get('/',  'QuestionController@index') ;  
	

// profile routes
Route::get('/profile', [
    'as' =>'profile.show',
    'uses' => 'ProfileController@show'
]);

Route::get('/profile/edit', [
    'as' =>'profile.edit',
    'uses' => 'ProfileController@edit'
]);

Route::post('/profile/edit', [
    'as' =>'profile.store',
    'uses' => 'ProfileController@store'
]);
// Authentication routes...
Route::get('auth/login', [
            'as' => 'login',
            'uses' => 'Auth\AuthController@getLogin'

    ]);

Route::post('auth/login', 'Auth\AuthController@postLogin');

Route::get('auth/logout', [
        'as' => 'logout',
        'uses' => 'Auth\AuthController@getLogout' ,
    ]);

// Registration routes...
Route::get('auth/register', [

    'as' => 'register',
    'uses' =>'Auth\AuthController@getRegister' ,
]);

Route::post('auth/register', 'Auth\AuthController@postRegister');


//questions routes


Route::resource('question' ,'QuestionController' ) ;

Route::get('question/tag/{id}', [
    'as' => 'question.tags' ,
    'uses' => 'QuestionController@questionsWithTag'
] ) ;

// tag routes

Route::resource('tag', 'TagController') ;

// responses routes

Route::resource('response' , 'ResponseController');

// comments routes



Route::post('comment', [
    'as' => 'comment.store' ,
    'uses'=> 'CommentController@store'
]);

Route::post('votes',  [
    'as' => 'vote.add',
    'uses' => 'VoteController@add'
]);

// favorite question  routes

Route::post('favorite/add',  'FavoriteController@add');
