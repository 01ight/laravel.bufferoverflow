<?php

namespace App\Events;

use App\Events\Event;
use App\Question;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class UserReachedMasterLevel extends Event
{
    use SerializesModels;
    public $user;

    /**
     * Create a new event instance.
     * @param Question $question
     */
    public function __construct(User $user  )
    {
            $this->user = $user  ;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
