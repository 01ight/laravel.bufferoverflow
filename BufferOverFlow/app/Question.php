<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected  $fillable= [
            "question_title" ,
            "question_body" ,
            "user_id"
        ] ;


     public function user()
     {
         return  $this->belongsTo('App\User') ;
     }

    public function  tags()
    {
        return  $this->belongsToMany('App\Tag') ;
    }
    public function responses ()
    {
        return  $this->hasMany('App\Response') ;
    }

    public function  comments () {
        return  $this->hasMany('App\Comment') ;
    }

    public function  favorites ()
    {
        return  $this->hasMany('App\Favorite') ;
    }


    public function  votes () {
        return $this->hasMany('App\Vote') ;
    }


}
