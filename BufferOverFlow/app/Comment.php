<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable  = [
        'comment_text' ,
        'question_id' ,
        'response_id' ,
    ] ;

    public function question()
    {
        return  $this->belongsTo('App\Question') ;
    }
    public function  response()
    {
        return  $this->belongsTo('App\Response') ;
    }
}
