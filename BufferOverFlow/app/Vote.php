<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{

    protected  $fillable = [
            'vote_value',
            'question_id',
            'response_id',
            'vote_type',
            'user_id'
    ] ;

    public function  user()
    {
        return  $this->belongsTo('App\User') ;
    }
    public function question()
    {
        return $this->belongsTo('App\Question') ;
    }
}
