@extends('layouts.layout')

@section('content')
    @if(session('updatedProfile'))
        <div class="alert alert-success">
            {{session('updatedProfile')}}
        </div>
    @endif


     <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class=" toppad  pull-right col-md-offset-3 ">
                                        <span class="label label-info">
                                                <a href="{{route('profile.edit')}}">edit</a>
                                        </span>
                <br>
                <p class=" text-info">{{$user->last_login_date}}</p>
            </div>
            <div class="col-md-12">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$user->login}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                <img alt="User Pic" src="{{$user->avatar}}" class="img-circle img-responsive">
                            </div>

                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>login</td>
                                        <td>{{$user->login}}</td>
                                    </tr>
                                    <tr>
                                        <td>first name</td>
                                        <td>{{$user->first_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>last name</td>
                                        <td>{{$user->last_name}}</td>
                                    </tr>
                                    <tr>
                                        <td>subscription date:</td>
                                        <td>{{$user->created_at}}</td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><a>{{$user->email}}</a></td>


                                    </tr>
                                    <tr>
                                       <td> badges</td>
                                        <td>
                                            @foreach ($badges as $badge )
                                                <span class=" {{$badge->badge_icon}}"></span>
                                            @endforeach
                                        </td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Favorite Questions</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>

                                    @foreach($favorites as $favorite)
                                        <tr>
                                            <td>
                                                <a href="{{route('question.show', $favorite->id)}}">
                                                    {{$favorite->question_title}}</a>
                                                <span class="label label-warning"> unfavorite</span>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Latest Questions</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class=" col-md-12 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    @foreach($questions as $question)
                                        <tr>
                                            <td><a href="{{route('question.show', $question['id'])}}">
                                                    {{$question['question_title']}}</a>
                                                <span class="label label-success">{{$question['created_at']}}</span>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Latest Answers</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class=" col-md-12 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    @foreach($responses as $response)
                                        <tr>
                                            <td><a href="{{route('question.show', $response['question_id'])}}">
                                                    {{$response['response_text']}}</a>
                                                <span class="label label-success">{{$response['created_at']}}</span>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop