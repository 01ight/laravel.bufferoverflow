@extends('layouts.layout')
@section('content')
        {!! Form::open (array('route' => 'profile.store', 'class'=> 'form-horizontal')) !!}

        <fieldset>
            <legend> Edit profile</legend>
            @if(session('editProfileError'))
                <div class="alert alert-success">
                    {{session('editProfileError')}}
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <label for="inputEmail" class="col-md-2 control-label">login</label>

                <div class="col-md-10">
                    <input type="text" class="form-control" name="login" value="{{$user->login}}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-md-2 control-label">First Name</label>

                <div class="col-md-10">
                    <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-md-2 control-label">Last Name</label>

                <div class="col-md-10">
                    <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="col-md-2 control-label">Avatar</label>

                <div class="col-md-10">
                    <input type="text" class="form-control" name="avatar" value="{{$user->avatar}}">
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword" class="col-md-2 control-label">Previews Password</label>

                <div class="col-md-10">
                    <input type="password" class="form-control" name="previews_password" placeholder="previews Password">
                </div>

            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-md-2 control-label">New Password</label>

                <div class="col-md-10">
                    <input type="password" class="form-control" name="new_password" placeholder="new Password">
                </div>

            </div>


            <div class="form-group">
                <label for="inputPassword" class="col-md-2 control-label">New Password confirmation</label>

                <div class="col-md-10">
                    <input type="password" class="form-control" name="new_password_confirm" placeholder="confirm Password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-raised btn-primary">Submit</button>
                </div>
            </div>
    </fieldset>
    {!!  Form::close()!!}
@stop