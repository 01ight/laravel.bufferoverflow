@extends('layouts.layout')

@section('sidebar')
  <h3> related questions </h3>
    @foreach($questions as $question )

        <div class="list-group-item">

                <div class="row-content">
                    <h4 class="list-group-item-heading">
                        <a href="{{route('question.show', $question->id )}}">
                            {{$question->question_title}}</a>
                    </h4>
                    <p class="list-group-item-text">{{str_limit($question->question_body, 20)}}</p>
                </div>
            </div>
        <div class="list-group-separator"></div>
    @endforeach
@stop