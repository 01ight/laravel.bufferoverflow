@extends('layouts.layout')

@section('content')
        <h1> list of all questions !! </h1>
        <ul class="list-group panel panel-info">
            @if(count($questions) ==0)
                <h3 style="height: 100px ; padding-left: 20px ; line-height: 3"> there is no question  for this category</h3>
            @endif
            @foreach ($questions as $question )

                <div class="row-fluid">

                    <div class="col-md-3">
                        <ul class="inline stats list-group">
                            <li class="list-group-item">
                                <span>{{$question->views}}</span>
                                views
                            </li>
                            <li class="list-group-item">
                                <span>
                                    @if(count($question->votes()->first()))
                                    {{$question->votes()->first()->toArray()['vote_value']}}
                                    @else {{0}}
                                    @endif
                                </span>
                                votes
                            </li>
                            <li class="list-group-item">
                                <span> {{$question->responses()->count()}}</span>
                                answers
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <h2>  <a href="{{route('question.show', $question->id )}}">
                                {{$question->question_title}}</a></h2>

                            <div class="row-content">{{str_limit($question->question_body, 200)}}
                                <br>
                                @foreach($question->tags()->get() as $tag )
                                    <span class="label label-info">{{$tag->tag_name}}</span>
                                @endforeach

                            </div>
                        <span class="label label-success"> created on : {{$question->created_at}}</span>

                    </div>
                </div>
                    <br><br>
                <div class="list-group-separator"></div>

            @endforeach
        </ul>

        {!! $questions->render()!!}

@stop