@extends('layouts.layout')

@section('content')
    <h1> ask a question </h1>


    <div class=" panel panel-info padding" >

            @if(session('questionCreated'))
                <div class="alert alert-info">
                    {{ session('questionCreated') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::open(array('route' => 'question.store', 'class' => 'form'))  !!}


            <div class="form-group">

                <input type="text" class="form-control"
                       name="question_title" placeholder="title ...">
            </div>

            <div class="form-group">
                    <textarea class="form-control" column="200"
                              name="question_body" id="question_body" placeholder="body ...">

                    </textarea>
            </div>

            @foreach($tags as $tag)

                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{{$tag->id}}" name="tags[]">
                            <span class="checkbox-material">
                            <span class="check">
                            </span>
                            </span>
                        {{$tag->tag_name}} <br>

                    </label>
                </div>
            @endforeach

            <div class="form-group">
                <button type="submit" class="btn btn-raised btn-primary">submit</button>
            </div>

            {!! Form::close() !!}
        </div>




@stop