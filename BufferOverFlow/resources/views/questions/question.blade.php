@extends('layouts.layout')
@section('content')


    @if(session('responseMade'))
        <div class="alert alert-success">
            {{session('responseMade')}}
        </div>
    @endif
    <div class="row">

    <div class="col-md-8">

        <div class="panel panel-info padding">

            <div class="question">
                <div class="row">
                    <div class="col-md-1">
                        <i class="fa fa-thumbs-up fa-2x vote-up-q "></i>
                        <p class="vote-value zero-margin">{{$question_vote}}</p>

                        <i class="fa fa-thumbs-down fa-2x vote-down-q "></i>

                        <i class="glyphicon glyphicon-ok  validate-question {{($question->validated)? 'validated' :''}}"></i>
                        <i class="fa  fa-2x favorite-question {{ ($favorited == 1) ? 'fa-star' :'fa-star-o' }}"></i>

                    </div>

                    <div class="col-md-11">
                        <h3> {{$question->question_title}} </h3>

                        <div>
                            {{$question->question_body}}

                        </div>

                        <br> <br>
                        @foreach($tags as $tag )
                            <span class="label label-info">{{$tag['tag_name']}}</span>
                        @endforeach
                        <div class="pull-right">

                <span>
                    <i>created by :</i>
                </span>
                     <span class="label label-default">
                        <i> {{$owner['login']}}</i>
                     </span>
                            <br>
                <span>
                    <i>on :</i>
                </span>
                     <span class="label label-default">
                    <i>  {{$owner['created_at']}}</i>
                         </span>
                        </div>

                        <br>
                        <br>
                        <hr class="alert-success">
                    </div>


                    <div id="question-comments-section" style="float:right">

                        <div class="question-comments-show">
                            @foreach($question_comments as $question_comment )
                                <div>
                                    <p> {{$question_comment->comment_text}}</p>
                            <span class="label label-default">
                                commented by :
                                <code> {{$question_comment->login}}</code>
                                on :
                                <code>  {{$question_comment->created_at}}</code>
                            </span>
                                </div>
                            @endforeach
                        </div>

                        <a href="javascript:void(0)"
                           class="btn btn-raised btn-primary add-question-comment zero-pad pull-right">add
                            comment</a>

                        {!! Form::open(array('route' => 'comment.store', 'class'=> 'form hide submit-comment' ))  !!}
                        <div class="form-group">
                            <input type="text" id="comment_text" name="comment_text" class="form-control">
                            <input type="hidden" id="question_id" name="question_id" value="{{$question->id }}">
                        </div>
                        <button type="submit" class="btn btn-sm">add</button>

                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>


                    <hr>
                </div>
            </div>

            @foreach($responses as $response )
                <div class="response">

                    <div class="row">
                        <div class="col-md-1">
                            <i class="fa fa-thumbs-up fa-2x vote-up-r "></i>
                            <p class="vote-value zero-margin">{{$question_vote}}</p>

                            <i class="fa fa-thumbs-down fa-2x vote-down-r "></i>
                        </div>

                        <div class="col-md-11">
                            <p>
                                {{$response->response_text}}
                            </p>

                        <span class="label label-default">
                            answered by :
                            <code> {{$response->login}}</code>
                            on :
                            <code>  {{$response->created_at}}</code>
                        </span>
                            <br><br>

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">

                                <div class="col-md-9 col-md-offset-3">

                                    <div id="response-comments-section " class="list-group">

                                        <div class="response-comments-show">
                                            @foreach($response_comments as $response_comment )
                                                @if($response_comment->id ==  $response->id)
                                                    <div class="list-group-item">
                                                        <div class="row-picture">
                                                            <img class="circle" src="{{
                                                        str_replace("300x300","300x300",$response_comment->avatar)}}"
                                                                 alt="icon" style="width: 27px;height: 27px;">
                                                        </div>
                                                        <div class="row-content">

                                                            <p class="list-group-item-text"> {{$response_comment->comment_text}}</p>
                                                        <span class="label label-info">
                                                            {{$response_comment->login}}
                                                        </span>
                                                            <span class="label label-info">
                                                                {{$response_comment->created_at}}
                                                            </span>

                                                        </div>
                                                        <div class="list-group-separator"></div>
                                                    </div>

                                                @endif
                                            @endforeach

                                        </div>

                                        <a href="javascript:void(0)"
                                           class="btn btn-raised btn-primary add-response-comment zero-pad pull-right">add
                                            comment</a>
                                        {!! Form::open(array('route' => 'comment.store', 'class'=> 'form hide submit-comment' ))  !!}
                                        <div class="form-group">
                                            <input type="text" id="comment_text" name="comment_text"
                                                   class="form-control"
                                                   placeholder="...">
                                            <input type="hidden" id="question_id" name="question_id"
                                                   value="{{$question->id }}">
                                            <input type="hidden" id="response_id" name="response_id"
                                                   value="{{$response->id }}">
                                        </div>
                                        <button type="submit" class="btn btn-sm">add</button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <hr>
            @endforeach


            <h4> add a response </h4>

            {!! Form::open(array('route' => 'response.store', 'class'=> 'form'))  !!}
            <div class="form-group">
                <textarea name="response_text" class="form-control" placeholder="...">
                </textarea>
                <input type="hidden" name="question_id" value="{{$question->id}}">
            </div>

            <button type="submit" class="btn btn-raised btn-success">respond</button>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-md-4">
        <ul class="list-group panel panel-info padding">
            @if(count($relatedQuestions) ==0)
                <h3 style="height: 100px ; padding-left: 20px ; line-height: 3"> there is no related question !!</h3>
            @endif
            @foreach ($relatedQuestions as $rQuestion )


                    <h2>  <a href="{{route('question.show', $rQuestion->id )}}">
                            {{$rQuestion->question_title}}</a></h2>

                    <div class="row-content">
                        {{str_limit($rQuestion->question_body, 100)}}

                    </div>
            @endforeach
        </ul>
    </div>
    </div>

@stop