@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <ul class="panel panel-info">
                <div class="panel-heading">
                    <h1 class="panel-title"> Register  </h1>
                </div>


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                {!! Form::open(array('route' => 'register', 'class' => 'form'))  !!}


                <div class="form-group">
                    login
                    <input type="text" name="login"  class="form-control" value="{{ old('login') }}">
                </div>

                <div class="form-group">
                    email
                    <input type="email" name="email"  class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    Password
                    <input type="password"  class="form-control" name="password">
                </div>

                <div class="form-group">
                    Confirm Password
                    <input type="password"  class="form-control"  name="password_confirmation">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn  btn-raised btn-primary">Register</button>
                </div>
                {!! Form::close() !!}
            </ul>
        </div>
    </div>

@stop