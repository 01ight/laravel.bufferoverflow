@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            <ul class="panel panel-info">
                <div class="panel-heading">
                    <h1 class="panel-title"> Login  </h1>
                </div>


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('route' => 'login', 'class' => 'form'))  !!}


                <div class="form-group">
                    Email
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    Password
                    <input type="password" class="form-control" name="password" id="password">
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember">
                   <span class="checkbox-material">
                       <span class="check">
                       </span>
                   </span>
                        Remember Me
                    </label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn  btn-raised btn-primary">Login</button>
                </div>
                {!! Form::close() !!}
            </ul>
        </div>

    </div>

@stop