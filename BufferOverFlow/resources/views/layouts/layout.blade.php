<!DOCTYPE html >

<html>
<head>
    <title> buffer over flow  !</title>
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.6/css/bootstrap-material-design.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="/css/user-profile.css" rel="stylesheet">
    <link href="/css/tag.css" rel="stylesheet">
    <link href="/css/flaticon.css" rel="stylesheet">
</head>

<body>
<div class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">BufferOverFlow</a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/question">Home</a></li>
                <li><a href="/question/create">ask question</a></li>
                <li><a href="/tag">tags</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control col-md-8" placeholder="Search">
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right">

                @if(Auth::check())

                    <li><a href="{{route('profile.show')}}"><i class="material-icons">account_box</i> {{Auth::user()->login}}</a></li>
                    <li><a href="{{route('logout')}}"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                @else
                    <li><a href="{{route('register')}}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="{{route('login')}}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>





      <div class="container">


          @if (session('mustLogIn'))
              <div class="alert alert-info">
                  {{ session('mustLogIn') }}
              </div>
          @endif

              @yield('content')



      </div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.6/js/material.min.js"></script>
<script src="/js/validate-question.js"></script>
<script src="/js/favorite-question.js"></script>

<script>

    $( document ).ready(function() {

            $('.add-response-comment, .add-question-comment').on('click', function ()
            {
                $(this).parent().find('form').removeClass('hide').slideDown(300) ;
            })
            $('form.submit-comment').submit(function( event ) {
                event.preventDefault() ;

                var form = $(this) ;
                var commentShowEl ;
                if($(this).hasClass('add-response-comment'))
                 commentShowEl = $(this).parent().find('.response-comments-show')   ;
                else
                    commentShowEl = $(this).parent().find('.question-comments-show')   ;


                console.log(commentShowEl) ;
                var comment_text  = $(this).find("input#comment_text").val() ;
                var question_id  = $(this).find("input#question_id").val() ;
                var response_id  = $(this).find("input#response_id").val() ? $(this).find("input#response_id").val() : 0;

                var CSRF_TOKEN = $('input[name="_token"]').val();
                    console.log(question_id + " " + response_id+ " " +CSRF_TOKEN) ;
                $.ajax({
                    type: "POST",
                    url: '/comment',
                    data: {
                        comment_text : comment_text,
                        question_id : question_id ,
                        response_id : response_id,
                        _token : CSRF_TOKEN
                    },
                    success: function (data){
                        console.log($.parseJSON(data)) ;
                        hideForm(form) ;
                        showComment(commentShowEl, $.parseJSON(data))
                    },
                    error : function (e) {
                     console.log(e)
                    }
                });
            })  ;

           function hideForm(formEl) {
                    formEl.hide() ;
           }

            function showComment(commentShowEl ,  commentData )
            {

                var commentText = $("<p></p>").val(commentData.comment_text) ;
                var commentAuthor = $('<span></span>').val(commentData.comment_author) ;
                var comment_created_at = $('<span></span>').val(commentData.comment_created_at.date) ;
                var  comment = $('<div></div>').append(commentText)
                                                .append(commentAuthor)
                                                .append(comment_created_at) ;
                commentShowEl.append(comment) ;
                $('body').append(commentShowEl) ;

            }
    }) ;

        $( document ).ready(function() {

            $('.vote-up-r, .vote-up-q').on('click' ,  function  () {

                var voteEl  = $(this).parent().find(".vote-value") ;
                var CSRF_TOKEN = $('input[name="_token"]').val();
                var question_id  = $("input#question_id").val() ;
                var response_id  = $(this).parent().find("#response_id").val() ;

                if($(this).hasClass('vote-up-q')) response_id= 0 ;


                submitVote(voteEl , 1, question_id,response_id, CSRF_TOKEN);
            });
            $('.vote-down-r, .vote-down-q').on('click' ,  function  () {

                var voteEl  = $(this).parent().find(".vote-value") ;
                var CSRF_TOKEN = $('input[name="_token"]').val();
                var question_id  = $("input#question_id").val() ;
                var response_id  = $(this).parent().find("input#response_id").val();

                if($(this).hasClass('vote-down-q')) response_id= 0 ;

                submitVote(voteEl , -1, question_id,response_id, CSRF_TOKEN);
            });
            function submitVote(voteEl , voteValue, question_id,response_id, CSRF_TOKEN ) {

                console.log(CSRF_TOKEN + " " + response_id) ;
                $.ajax({
                    type: "POST",
                    url: '/votes',
                    data: {
                        vote_value: voteValue,
                        question_id :question_id,
                        response_id :response_id,
                        _token: CSRF_TOKEN
                    },
                    success: function (val) {
                        console.log(val);
                        showVote(voteEl, val) ;
                    },
                    error: function (e) {
                        console.log(e)
                    }
                })
            }

            function  showVote(voteValueEl , voteValue) {
                console.log(voteValueEl)
                voteValueEl.text(voteValue);
            }

        })
</script>
</body>
</html>


