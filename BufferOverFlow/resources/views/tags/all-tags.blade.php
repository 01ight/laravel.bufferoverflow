@extends('layouts.layout')

@section('content')

    <h1> All Tags   </h1>

    <div class="row">
        <div class="col-md-12">
            <ul class="event-list">
                @foreach ($tags as $tag )
                <li>
                    <time datetime="2014-07-20">
                        <span class="day"></span>
                        <span class="month">
                            <a style = " color: rgb(255, 255, 255) ;" href="{{route('question.tags' , $tag->id)}}">{{$tag->tag_name}}
                            </a></span>

                    </time>

                    <div class="info">
                        <p class="desc">{{$tag->tag_description}}</p>
                    </div>

                </li>
                @endforeach

            </ul>
        </div>
    </div>
@stop