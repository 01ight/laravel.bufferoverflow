@extends('layouts.layout')

@section('content')

    <div class="col-md-6">
        @if (session('tagCreated'))
            <div class="alert alert-info">
                {{ session('tagCreated') }}
            </div>
        @endif
    <h1> create a new tag !! </h1>

    {!! Form::open(array('route' => 'tag.store', 'class' => 'form')) !!}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="tag name ... " name ="tag_name">
        </div>
        <div class="form-group">
            <textarea  class="form-control" placeholder="tag description ..." name="tag_description">
            </textarea>
        </div>
        <button  type="submit" class="btn  btn-info" >add</button>
    {!! Form::close()  !!}

    </div>


@stop